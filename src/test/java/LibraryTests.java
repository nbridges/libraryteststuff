import com.mastercontrol.nbridges.javatest.library.Library;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by nbridges on 12/9/2016.
 */
public class LibraryTests {
  Library myObj = new Library();

  @Test
  public void addTest() {
    double stuff = myObj.add(10, 5);
    assertEquals(15, stuff, 0);
  }

  @Test
  public void subtractTest() {
    double stuff2 = myObj.subtract(10, 5);
    assertEquals(5, stuff2, 0);
  }

  @Test
  public void multiplyTest() {
    double stuff3 = myObj.multiply(10, 5);
    assertEquals(50, stuff3, 0);
  }

  @Test
  public void divideTest() {
    double stuff4 = myObj.divide(10, 5);
    assertEquals(2, stuff4, 0);
  }
}
