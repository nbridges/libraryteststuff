package com.mastercontrol.nbridges.javatest.library;

/**
 * Created by nbridges on 12/9/2016.
 */
public class Library {

    private double result;

    /**
     * This function adds.
     * @param a input variable
     * @param b input variable
     * @return result
     */
    public double add(final double a, final double b) {
        result = a + b;
        return result;
    }

    /**
     * This function subtracts.
     * @param a input variable
     * @param b input variable
     * @return result
     */
    public double subtract(final double a, final double b) {
        result = a - b;
        return result;
    }

    /**
     * This function multiplies.
     * @param a input variable
     * @param b input variable
     * @return result
     */
    public double multiply(final double a, final double b) {
        result = a * b;
        return result;
    }

    /**
     * This function divides.
     * @param a input variable
     * @param b input variable
     * @return result
     */
    public double divide(final double a, final double b) {
        if (b == 0) {
            System.out.println("Error: Divide by zero!");
            return 0;
        } else {
            result = a / b;
            return result;
        }
    }
}
